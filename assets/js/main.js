
$(document).ready(() => {
  const host = window.location.href;
  var zip = new JSZip();

  $('.js-download-all').on('click', (e) => {
    e.preventDefault();
    let folder = zip.folder('CS Download');
    $('.js-download-item').each((index, item) => {
      const fileLink = `${host}/${$(item).attr('href')}`;
      const fileExt = $(item).data('type');
      const fileName = `${$(item).text()}.${fileExt}`;
      JSZipUtils.getBinaryContent(fileLink, function (err, data) {
         if(err) {
            throw err;
         }
         folder.file(`${fileName}.fileExt`, data, {binary:true});
      });
    });
    console.log(folder);
    zip.generateAsync({type : "blob"})
      .then(function(content) {
        saveAs(content, "example.zip");
      });
  });
});
